module.exports = {
    name: 'traitor',
    aliases: ['t','tr'],
	description: 'The command for social deduction games',
    args: false,
    usage: '[args] [args], you may input the following arguments\ndraw - draw traitors\nst - Secret Traitors. Traitors don\'t know who the other traitors are\ntasks - enable tasks\n# - how many traitors to draw, by default this is 1.',
	execute(message, args) {
        // set a traitor
        var traitorAmount = 1; // minimum number of traitors to select
        var playerArray = []; // will store all players, and eventually just innocent players
        var traitorArray = []; // stores all traitors
        var confirmedPlayers = ''; // initialize string for the players in game
        var traitorNames = ''; // initialize string for traitors
        var murderChannel = 'channelID Here'; // channel id to send game messages to
        var namedPlayerArray = [{name:'Thing 1', arg:'t1', id:'discordIDHere'},
                                {name:'Thing 2', arg:'t2', id:'discordIDHere'}];
        var taskNum = 3; // number of tasks given to each player
        var taskDelay = 10 * delay; // number of seconds each task will take
        var delay = 1000; // convert seconds to milliseconds
        var gameDelay = 15 * delay; // number of seconds to wait after sending roles for game start
        var taskBoolean = false; // if true, tasks will be sent out
        var taskArray = ['Sit and grab a **Snack** in the **Cafeteria**',
                         'Grab an **Assault Rifle** at **Main Arsenal**',
                         'Grab a **Pistol** at **Pistol Rack**',
                         '\'Use\' the **Console** in **Lower Vents**',
                         '\'Use\' a **Console** in **Blue Blast Door Room**',
                         'Jump through the **Ray Shield** near **Blue Blast Door Room**',
                         '**Board** the **Monorail Car**',
                         '\'Maintain\' the **Rear Grate** in **Closed Monorail**',
                         '\'Use\' the **Console** in **Monorail Terminal**',
                         'Rest on a **Bunk** in **Barracks**',
                         'Check the **Desk** in **Barracks Office**',
                         'Board an **Escape Pod** in **Main Hall**',
                         'Punch three **Crates** in **Cargo**',
                         'Stare into **Space** **[Wildcard]**',
                         'Clean up two **Blood Spills** **[Wildcard]**',
                         'Maintain three **Vent Grates** **[Wildcard]**'];

        function argsToId(potentialPlayer){ // checks the shortcut names, and if a player adds it to the list of confirmed players
            for(var i = 0; i < namedPlayerArray.length; i++){
                if(potentialPlayer == namedPlayerArray[i].arg){
                    playerArray.push(namedPlayerArray[i].id);
                    confirmedPlayers = confirmedPlayers + namedPlayerArray[i].name + ' ';
                }
            }
        }

        function delayedTask(message){
            message.reply('Task completed!');
            client.channels.cache.get(murderChannel).send('A Task has been Completed!');
        }
        function notifyGameStart(target){
            setTimeout(function() { target.send('Game Start!'); }, gameDelay);
        }

        argsCheckerLoop: {
        var guestNum = 1;
        for (var i = 0; i < args.length; i++) {
                // if integer is provided, game accepts that as the level
                if (Number.isInteger(Math.floor(Number(args[i])))) {
                    // if it's less that 11, make it a minimum of 1, but use it for traitoramount
                    let inputInt = Math.floor(Number(args[i]));
                    // checks if an arg is an int for number of traitors
                    if(inputInt <= playerArray.length && inputInt > 0){
                        traitorAmount = inputInt;
                    } else if (inputInt > 1000000000000000) {
                        var stringedInt = args[i];
                        //let stringedInt = '180796799592693761';
                        let tempUser = message.guild.members.fetch(stringedInt).then(console.log).catch(console.error);
                        // if inputInt looks like a client ID, push it to namedPlayerArray
                        namedPlayerArray.push( {name: ('Guest ' + guestNum), id: stringedInt } );
                        guestNum++;
                        // and push the id as a string to playerArray
                        playerArray.push(stringedInt);
                        // add the guest to the confirmed players
                        confirmedPlayers = confirmedPlayers + ' ' + namedPlayerArray[namedPlayerArray.length - 1].name;
                        console.log('Guest added!\n' + namedPlayerArray[namedPlayerArray.length - 1].name + ' id: ' + stringedInt );
                    }
                } else {
                    // if the arg is a name shortcut, add them
                    argsToId(args[i]);
                }
            }
        }

        if (args.indexOf("tasks") != -1) {
            // enables tasks if tasks is a listed arg
            taskBoolean = true;
        }

        if (args.indexOf("draw") != -1) {
            console.log(traitorAmount);
            for (var i = 0; i < traitorAmount; i++){
                // pull a random index from playerarray
                var draw = Math.floor(Math.random() * playerArray.length);
                // pushes id from playerArray to traitorArray
                traitorArray.push(playerArray[draw]);
                // remove drawn id from playerArray
                playerArray.splice(draw, 1);
            }
            for(var j = 0; j < traitorArray.length; j++){
                // get user from id
                traitorNames = '';
                var traitor = message.guild.members.cache.get(traitorArray[j]);
                // make the list of traitor names to add
                for(var k = 0; k < traitorArray.length; k++){
                    // each traitor might need to know who all the other traitors are
                    indivTraitor = message.guild.members.cache.get(traitorArray[k]);
                    for(var l = 0; l < namedPlayerArray.length; l++){
                        // go through the list and add every traitor name to the list of traitors
                        if(traitorArray[k] == namedPlayerArray[l].id){
                            traitorNames = traitorNames + namedPlayerArray[l].name + ' ';
                        }
                    }
                }
                if (args.indexOf("st") != -1) {
                    // if secret traitor is on, don't message the other traitor names
                    traitor.send('You are a **Traitor**');
                    notifyGameStart(traitor);
                } else {
                    //otherwise, send the other traitor names
                    traitor.send('You are a **Traitor**, the traitors are: ' + traitorNames);
                    notifyGameStart(traitor);
                }
            }
            for(var j = 0; j < playerArray.length; j++){
                // get user from id
                var innocent = message.guild.members.cache.get(playerArray[j]);
                // initialize the list of tasks to send to the innocent
                var innocentTasks = '';
                // draw a number of tasks equal to taskNum
                var indivTaskArray = taskArray;
                for (var k = 0; k < taskNum; k++){
                    var taskDraw = Math.floor(Math.random() * indivTaskArray.length);
                    innocentTasks = innocentTasks + indivTaskArray[taskDraw] + '\n\n';
                    // once a task is drawn, remove it from the task array
                    indivTaskArray.splice(taskDraw, 1);
                }
                if (taskBoolean){
                    // if tasks are enabled, give the innocent their tasks
                    innocent.send('You are an **Innocent**, your tasks are:\n⠀\n' + innocentTasks + 'message me \'**<prefix>.t task**\' to confirm task completion!');
                    notifyGameStart(innocent);
                } else {
                    // or don't, I guess
                    innocent.send('You are an **Innocent**');
                    notifyGameStart(innocent);
                }
            }
            // inside the murder channel, send the list of players
            client.channels.cache.get(murderChannel).send('The players this round are: **' + confirmedPlayers + '**');
        }
        // handles the task confirmation dms
        if ((message.channel.type === 'dm') && (args.indexOf("task") != -1)) {
            setTimeout(function() { message.reply('Task completed!'); }, taskDelay);
            setTimeout(function() { client.channels.cache.get(murderChannel).send('A Task has been Completed!'); }, taskDelay);
        }
    },
    };

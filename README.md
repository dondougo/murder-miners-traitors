# Murder Miners Traitors

This is the command file for discord bots to play Traitors in Murder Miners. Just drag the file into your commands folder!

If you need a bot that can run this, I think https://www.sitepoint.com/discord-bot-node-js/ should tell you how to make one.



This bot is by me, Dondougo!

Holler if anything's acting up: https://twitter.com/TheDondougo

Check the video for details: https://youtu.be/_cLDh7ArSA4

Like this? Check out my Youtube channel! https://youtube.com/user/Dondougo



For the draw command, here's the recommended structure:

**prefix.traitor  draw  (name/ID)  (name/ID)  st  (traitor#)**

Basically, draw and st can go anywhere, but the number of traitors needs to be the last thing.

- Game Settings
- Mode: Murdermatch
- Map: Dead Space
- Time Limit: None
- Locked Blocks: 100%
- Radar Mode: Motion
- 1 Life Mode: On
- Weapons Enabled:
- Pistol
- Machine Gun
- Shotgun
- Battle Rifle
- Sniper Rifle
- Hookshot

Innocents, Options > Debug Options > HUD: Off


Any updates to the bot will go here, but generally this website will not be supported.

- 10-17-2020 - Uploaded original
- 10-26-2020 - Fixed error where inputting a discord ID wouldn't add that player to the game